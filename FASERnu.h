//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Mar 25 15:10:43 2021 by ROOT version 5.34/38
// from TTree FASERnu/
// found on file: FASERnu_numu.dump._001-100.root
//////////////////////////////////////////////////////////

#ifndef FASERnu_h
#define FASERnu_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class FASERnu {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        row_wise_branch_e_beam;
   Int_t           row_wise_branch_id_beam;
   Double_t        row_wise_branch_x_beam;
   Double_t        row_wise_branch_y_beam;
   Int_t           row_wise_branch_pdgnu_nuEvt;
   Int_t           row_wise_branch_pdglep_nuEvt;
   Double_t        row_wise_branch_Enu_nuEvt;
   Double_t        row_wise_branch_Plep_nuEvt;
   Int_t           row_wise_branch_cc_nuEvt;
   Double_t        row_wise_branch_x_nuEvt;
   Double_t        row_wise_branch_y_nuEvt;
   Double_t        row_wise_branch_z_nuEvt;
   Int_t           row_wise_branch_chamber_count;
   Int_t           row_wise_branch_chamber[46592];   //[chamber_count]
   Int_t           row_wise_branch_iz_count;
   Int_t           row_wise_branch_iz[46592];   //[iz_count]
   Int_t           row_wise_branch_izsub_count;
   Int_t           row_wise_branch_izsub[46592];   //[izsub_count]
   Int_t           row_wise_branch_pdgid_count;
   Int_t           row_wise_branch_pdgid[46592];   //[pdgid_count]
   Int_t           row_wise_branch_id_count;
   Int_t           row_wise_branch_id[46592];   //[id_count]
   Int_t           row_wise_branch_idParent_count;
   Int_t           row_wise_branch_idParent[46592];   //[idParent_count]
   Int_t           row_wise_branch_charge_count;
   Double_t        row_wise_branch_charge[46592];   //[charge_count]
   Int_t           row_wise_branch_x_count;
   Double_t        row_wise_branch_x[46592];   //[x_count]
   Int_t           row_wise_branch_y_count;
   Double_t        row_wise_branch_y[46592];   //[y_count]
   Int_t           row_wise_branch_z_count;
   Double_t        row_wise_branch_z[46592];   //[z_count]
   Int_t           row_wise_branch_px_count;
   Double_t        row_wise_branch_px[46592];   //[px_count]
   Int_t           row_wise_branch_py_count;
   Double_t        row_wise_branch_py[46592];   //[py_count]
   Int_t           row_wise_branch_pz_count;
   Double_t        row_wise_branch_pz[46592];   //[pz_count]
   Int_t           row_wise_branch_e1_count;
   Double_t        row_wise_branch_e1[46592];   //[e1_count]
   Int_t           row_wise_branch_e2_count;
   Double_t        row_wise_branch_e2[46592];   //[e2_count]
   Int_t           row_wise_branch_len_count;
   Double_t        row_wise_branch_len[46592];   //[len_count]
   Int_t           row_wise_branch_edep_count;
   Double_t        row_wise_branch_edep[46592];   //[edep_count]

   // List of branches
   TBranch        *b_row_wise_branch;   //!

   FASERnu(TTree *tree=0);
   virtual ~FASERnu();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef FASERnu_cxx
FASERnu::FASERnu(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("FASERnu_numu.dump._001-100.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("FASERnu_numu.dump._001-100.root");
      }
      f->GetObject("FASERnu",tree);

   }
   Init(tree);
}

FASERnu::~FASERnu()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t FASERnu::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t FASERnu::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void FASERnu::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("row_wise_branch", &row_wise_branch_e_beam, &b_row_wise_branch);
   Notify();
}

Bool_t FASERnu::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void FASERnu::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t FASERnu::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef FASERnu_cxx
