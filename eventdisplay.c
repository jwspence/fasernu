#include <stdlib.h>
#include <iostream>
#include "TFile.h"
#include "TString.h"
#include "TNtuple.h"
#include "TLeaf.h"
#include "TH3D.h"
#include "TPolyLine3D.h"
using namespace std;
void eventdisplay(TString filename,int lower,int upper){
	TH3D *hframe = new TH3D("hframe","FASERnu Event Display",100,-700,-600,100,-125,125,100,-125,125);
	TFile *f = new TFile(filename);
	TNtuple *nt = (TNtuple*)f->Get("FASERnu");
	TBranch *b = nt->FindBranch("row_wise_branch");
	TLeaf *xleaf = nt->FindLeaf("x");
	TLeaf *yleaf = nt->FindLeaf("y");
	TLeaf *zleaf = nt->FindLeaf("z");
	TLeaf *izsubleaf = nt->FindLeaf("izsub");
	TLeaf *chargeleaf = nt->FindLeaf("charge");
	TLeaf *pxleaf = nt->FindLeaf("px");
	TLeaf *pyleaf = nt->FindLeaf("py");
	TLeaf *pzleaf = nt->FindLeaf("pz");
	int nhits;
	gStyle->SetOptStat(0);
	hframe->GetXaxis()->SetTitle("x(mm)");
	hframe->GetYaxis()->SetTitle("y(mm)");
	hframe->GetZaxis()->SetTitle("z(mm)");
	hframe->Draw();
	for(int i=lower;i<upper;i++){
	b->GetEntry(i);
	nhits = zleaf->GetLen();
	int charge;
	double x, y, z, izsub, px, py, pz, p;
	for(int j=0;j<nhits;j++){
		charge = chargeleaf->GetValue(j);
		if(charge==0)continue;
		izsub = izsubleaf->GetValue(j);
		if(izsub!=0)continue;
		x = xleaf->GetValue(j);
		y = yleaf->GetValue(j);
		z = zleaf->GetValue(j);
		px = pxleaf->GetValue(j);
		py = pyleaf->GetValue(j);
		pz = pzleaf->GetValue(j);
		p = sqrt(px*px+py*py+pz*pz);
//		if(p<100)continue;
//		cout << "(" << x << "," << y << "," << z << ")" << endl;
//		cout << "(" << px << "," << py << "," << pz << ")" << endl;
		TPolyLine3D *l = new TPolyLine3D();
		l->SetPoint(0,z,x,y);
		l->SetPoint(1,z+0.3*pz/p,x+0.3*px/p,y+0.3*py/p);
		l->Draw("same");
	}
}

}
