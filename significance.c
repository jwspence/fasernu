void significance(){
	TFile *fsig = new TFile("0-parameters-sig.root");
	TFile *fbg = new TFile("0-parameters-bg.root");
	TH1D *hsig = (TH1D*)fsig->Get("alpha");
	TH1D *hbg = (TH1D*)fbg->Get("alpha");
	TH1D *hsignificance = new TH1D("significance","Significance",100,0,125);
	TCanvas *c = new TCanvas();
	double hsigintegral = hsig->Integral(0,100);
	double hbgintegral = hbg->Integral(0,100);
	hsig->Scale(1/hsigintegral);
	hbg->Scale(1/hbgintegral);
	for(int i=1;i<100;i++){
		hsigintegral = hsig->Integral(0,i);
		hbgintegral = hbg->Integral(0,i);
		hsignificance->SetBinContent(i,hsigintegral/sqrt(hbgintegral));


	}
	hsignificance->Draw();
}
