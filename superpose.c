#include <TFile.h>
#include <TString.h>
#include <TH1.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <iostream>
#include <stdlib.h>
void superpose(TString sigfile,TString bgfile){
	TFile *fbg = new TFile(bgfile);
	TFile *fsig = new TFile(sigfile);
	for(int i=0;i<100;i++){
		TString string = Form("energy%d",i);
		std::cout << "string = " << string << std::endl;
		TH1D *hbg = (TH1D*)fbg->Get(string);
		TH1D *hsig = (TH1D*)fsig->Get(string);
		double hbgintegral = hbg->Integral(0,hbg->GetNbinsX());
		double hsigintegral = hsig->Integral(0,hsig->GetNbinsX());
		hbg->Scale(1/hbgintegral);
		hsig->Scale(1/hsigintegral);
		hbg->SetLineColor(kRed);
		hsig->SetLineColor(kBlack);
		hbg->GetXaxis()->SetTitle("iz");
		hbg->GetXaxis()->SetTitle("tracks");
		TCanvas *c = new TCanvas();
		TLegend *legend = new TLegend(0.7,0.65,0.9,0.75);
		legend->AddEntry(hsig,"Signal (e)");
		legend->AddEntry(hbg,"Background (#mu)");
		hbg->Draw();
		hsig->Draw("same");
		legend->Draw();
		c->SaveAs(string+".png");
	}
}
