#include <TFile.h>
#include <TBranch.h>
#include <stdlib.h>
void writettree(){
   Int_t	iz;
   Int_t	PDGID;
   Int_t	charge;
   Double_t	x;
   Double_t	y;
   Double_t	z;
   Double_t	px;
   Double_t	py;
   Double_t	pz;


   TFile *hfile = new TFile("skim.root","RECREATE");

   TTree *tree = new TTree("dataset","Skimmed data set");
   tree->Branch("iz",&iz,"iz/I");
   tree->Branch("PDGID",&PDGID,"PDGID/I");
   tree->Branch("charge",&charge,"charge/I");
   tree->Branch("x",&x,"x/D");
   tree->Branch("y",&y,"y/D");
   tree->Branch("z",&z,"z/D");
   tree->Branch("px",&px,"px/D");
   tree->Branch("py",&py,"py/D");
   tree->Branch("pz",&pz,"pz/D");
   tree->Write();
   hfile->Close();
}