#include <TFile.h>
#include <TNtuple.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TROOT.h>
#include <TH1.h>
#include <iostream>

void readfile(TString input, TString output){
	TFile *f = new TFile(input);
	TNtuple* ntread = (TNtuple*)f->Get("FASERnu");
	if(!ntread)cout << "NTuple not found" << endl;
	TBranch *b = ntread->FindBranch("row_wise_branch");
	if(!b)cout << "row_wise_branch not found" << endl;
	gROOT->cd();
	TNtuple *ntwrite = new TNtuple("ntuple","","energy:trackangle");
	TH1D htrackangle("trackangle","trackangle",100,0,1.58);
	TH1D *histtrackanglearray[100];
	for(int idx=0;idx<100;idx++){
		histtrackanglearray[idx] = (TH1D*)htrackangle.Clone(Form("trackangle%d",idx));
	}
	TH1D henergy("energy","energy",100,0,1000);
	TH1D *histenergyarray[100];
	for(int idx=0;idx<100;idx++){
		histenergyarray[idx] = (TH1D*)henergy.Clone(Form("energy%d",idx));
	}
	int nentries = b->GetEntries();
	for(int i=0;i<nentries;i++){
		b->GetEntry(i);
		TLeaf *xleaf = b->FindLeaf("x");
		TLeaf *yleaf = b->FindLeaf("y");
		TLeaf *zleaf = b->FindLeaf("z");
		TLeaf *pxleaf = b->FindLeaf("px");
		TLeaf *pyleaf = b->FindLeaf("py");
		TLeaf *pzleaf = b->FindLeaf("pz");
		TLeaf *pdgidleaf = b->FindLeaf("pdgid");
		TLeaf *chargeleaf = b->FindLeaf("charge");
		TLeaf *izleaf = b->FindLeaf("iz");
		TLeaf *izsubleaf = b->FindLeaf("izsub");
		int nhits = xleaf->GetLen();
		int pdgid, charge, iz, izsub;
		double x, y, z, px, py, pz, p, trackangle;
		cout << "Processing event " << i << endl;
		for(int j=0;j<nhits;j++){
			pdgid = pdgidleaf->GetValue(j);
			if(pdgid==13||pdgid==-13)continue;
			x = xleaf->GetValue(j);
			y = yleaf->GetValue(j);
			z = zleaf->GetValue(j);
			px = pxleaf->GetValue(j);
			py = pyleaf->GetValue(j);
			pz = pzleaf->GetValue(j);
			p = sqrt(px*px+py*py+pz*pz);
			trackangle = atan(sqrt(px*px+py*py)/pz);
			charge = chargeleaf->GetValue(j);
			iz = izleaf->GetValue(j);
			izsub = izsubleaf->GetValue(j);
//			cout << i << "    " << p << endl;
			ntwrite->Fill(p,trackangle);
			if(iz<100){
				histtrackanglearray[iz]->Fill(trackangle);
				histenergyarray[iz]->Fill(p);
			}
		}
	}
	TFile *g = new TFile(output,"RECREATE");
	ntwrite->Write();
	for(int idx=0;idx<100;idx++){
		histtrackanglearray[idx]->Write();
		histenergyarray[idx]->Write();
	}
	g->Close();
}
