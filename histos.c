#include <stdlib.h>
#include <iostream>
#include "TFile.h"
#include "TString.h"
#include "TNtuple.h"
#include "TLeaf.h"
#include "TH3D.h"
#include "TPolyLine3D.h"
using namespace std;
void histos(TString filename,int lower,int upper){
	TH1D *halpha = new TH1D("alpha","alpha",100,0,1.57);
	TH1D *hdt = new TH1D("dt","dt",100,0,1.57);
	TH1D *htx = new TH1D("tx","tx",100,0,1.57);
	TH1D *hty = new TH1D("ty","ty",100,0,1.57);
	TH1D *hip = new TH1D("ip","ip",100,0,125);
	TFile *f = new TFile(filename);
	TNtuple *nt = (TNtuple*)f->Get("FASERnu");
	TBranch *b = nt->FindBranch("row_wise_branch");
	TLeaf *xleaf = nt->FindLeaf("x");
	TLeaf *yleaf = nt->FindLeaf("y");
	TLeaf *zleaf = nt->FindLeaf("z");
	TLeaf *izsubleaf = nt->FindLeaf("izsub");
	TLeaf *chargeleaf = nt->FindLeaf("charge");
	TLeaf *pdgidleaf = nt->FindLeaf("pdgid");
	TLeaf *pxleaf = nt->FindLeaf("px");
	TLeaf *pyleaf = nt->FindLeaf("py");
	TLeaf *pzleaf = nt->FindLeaf("pz");
	int nhits;
	for(int i=lower;i<upper;i++){
	b->GetEntry(i);
	cout << "event " << i << endl;
	nhits = zleaf->GetLen();
	int charge, pdgid;
	double x, y, z, izsub, px, py, pz, p, alpha, dt, tx, ty, ip;
	for(int j=0;j<nhits;j++){
		charge = chargeleaf->GetValue(j);
		if(charge==0)continue;
		izsub = izsubleaf->GetValue(j);
		if(izsub!=0)continue;
		pdgid = pdgidleaf->GetValue(j);
		if(pdgid==13||pdgid==-13)continue;
		x = xleaf->GetValue(j);
		y = yleaf->GetValue(j);
		z = zleaf->GetValue(j);
		alpha = atan(sqrt(x*x+y*y)/(z+695.5));
		if(alpha>0.2)continue;
		px = pxleaf->GetValue(j);
		py = pyleaf->GetValue(j);
		pz = pzleaf->GetValue(j);
		p = sqrt(px*px+py*py+pz*pz);
		dt = atan(sqrt(px*px/(pz*pz)+py*py/(pz*pz)));
		tx = atan(sqrt(px*px/(pz*pz)));
		ty = atan(sqrt(py*py/(pz*pz)));
		ip = sqrt((x-(z+695.5)*tan(tx))*(x-(z+695.5)*tan(tx))+(y-(z+695.5)*tan(ty))*(y-(z+695.5)*tan(ty)));
		halpha->Fill(alpha);
		hdt->Fill(dt);
		htx->Fill(tx);
		hty->Fill(ty);
		hip->Fill(ip);
	}
}
TFile *w = new TFile("parameters.root","RECREATE");
halpha->Write();
hdt->Write();
htx->Write();
hty->Write();
hip->Write();
w->Close();
}
